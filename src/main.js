import React from "react";
import ReactDOM from "react-dom";
import Row from "./rowComponent";

var MainApp = React.createClass({
  getInitialState: function() {
    return {
      orderList: [ 
      {
        "name": "",
        "qty": 1, 
        "price": 0,
        "total": 0
      } ]
    }
  },

  addRow: function( ) {
    var item = {name: "", qty: 1, price: 0, total: 0 }
    this.setState({orderList: this.state.orderList.concat([item])})
  },

  render: function() {
   
    return (
      <div> 
        <div className="topRow">
          <label>Name</label> 
          <label>Quantity</label>
          <label>Price</label>
          <label>Total</label>
        </div>

        {this.state.orderList.map((item, i) => {
          return(
            <div key={i}> 
              <Row key={i} name={item.name} qty={item.qty}  price={item.price} total={item.total}></Row>  
            </div>
          )
        })}

        <button onClick= {this.addRow}>New Order Line</button>
      </div>
    )  
  }
});

ReactDOM.render(<MainApp/>, document.getElementById("app"));