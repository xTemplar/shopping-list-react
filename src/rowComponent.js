import React from "react";
import ReactDOM from "react-dom";
import { getProducts } from "./api";

     
var Row = React.createClass({

  getInitialState: function() {
    return {
      name: "",
      qty: 1,
      price: 0,
      upToDate: true,
      focus: false,
      disableInput: false,
      reqId: 0,
      subList: [] 
     }
  },

  handleNameChange: function(event) {

    var eventVal=event.target.value;
    this.setState({
      name: eventVal,
      price: 0,
      focus: true,
      reqId: (this.state.reqId + 1),
      subList: []
    });
    
    if(eventVal == ""){
      this.setState({  
        subList: [],
        disableInput: false
      });
      
    } else{
        var tempId= this.state.reqId +1 ; 
        
        this.setState({ upToDate: false });

        getProducts(eventVal, ps => { 
          
          if(tempId == this.state.reqId ) { 
            this.setState({
              upToDate: true,
              disableInput: false
            });

            if(this.state.focus) {

              this.setState({ subList: ps });

            } else if(ps.length>0){

              this.setState({
                name: ps[0].name,
                price: ps[0].price,
                subList: []
              });
            }
          } 
        }); 
    }
  },

  handleQtyChange: function(event) {

    var eventVal= event.target.value;

    if( !isNaN(parseFloat(eventVal)) && isFinite(eventVal) ){
       this.setState({ qty: eventVal });
    }
  },
  
  handleBlur: function() {

      this.setState({ focus: false });

      if(this.state.upToDate && this.state.subList.length > 0){
        this.setState({
          name: this.state.subList[0].name,
          price: this.state.subList[0].price,
          subList: []
        });
      } else if(this.state.name != "" && !this.state.upToDate){
        this.setState({
          disableInput: true,
          subList: []
        });
      }
  },

  render: function() {
    return (
      <div className= "row">
        <input value={this.state.name} onChange={this.handleNameChange} disabled={this.state.disableInput} onBlur={this.handleBlur} />
        <input value={this.state.qty} onChange={this.handleQtyChange}/> 
        <ol className="completions">
          {this.state.subList.map((item, i)=> {
            return(
              <li key={i}>{item.name}</li> 
            )
          })}
        </ol>  
        <label>{this.state.price} </label> 
        <label>{this.state.price * this.state.qty}</label> 
      </div>
    )  
  }
});

export default Row;