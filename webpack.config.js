const path = require('path');
const webpack = require('webpack');

const isDev = (process.env.NODE_ENV || JSON.stringify('development')) === 'development';

module.exports = {
    cache: isDev,
    entry: [
        // Set up an ES6-ish environment
        'babel-polyfill',
        // application script
        path.resolve('./src/main.js')
    ],
    resolve: {extensions: ['', '.js', '.jsx']},
    resolveLoader: {root: path.join(__dirname, 'node_modules')},
    output: {
        path: path.resolve('./public/bin'),
        publicPath: '/scripts',
        filename: 'app.min.js'
    },
    devServer: {
    contentBase: "./public"
   
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                    presets: ['react', 'es2015', 'stage-0']
                }
            },
            { test: /\.less$/, loader: 'style!css!postcss?browsers=last 2 versions!less' }
        ]
    },
    bail: true,
    stats: {colors: true},
    devtool: isDev ? '#eval-source-map' : '#hidden-source-map',
    debug: isDev
};
